import kivy
kivy.require('1.10.0')
from kivy.app import App
from kivy.lang import Builder
from kivy.clock import Clock
from kivy.uix.screenmanager import Screen

class FirstScreen(Screen):
    pass

class SecondScreen(Screen):
    pass

class ThirdScreen(Screen):
    pass

class FourthScreen(Screen):
    pass

root_widget = Builder.load_string("""
ScreenManager:
    FirstScreen:
    SecondScreen:
    ThirdScreen:
    FourthScreen:
<FirstScreen>:
    first_screen: first_screen
    name: '_first_screen_'
    Label:
        id: first_screen
        text: "Hi I'm The First Screen"
<SecondScreen>:
    name: '_second_screen_'
    Label:
        id: second_screen
        text: "Hi I'm The Second Screen"
<ThirdScreen>:
    name: '_third_screen_'
    Label:
        id: third_screen
        text: "Hi I'm The Third Screen"
<FourthScreen>:
    name: '_fourth_screen_'
    Label:
        id: fourth_screen
        text: "Hi I'm The Fourth Screen"
""")

class SwitchingScreenApp(App):
    def build(self):
        Clock.schedule_once(self.screen_switch_one, 2)
        Clock.schedule_once(self.screen_switch_two, 4)
        Clock.schedule_once(self.screen_switch_three, 6)
        Clock.schedule_once(self.screen_switch_four, 8)
        # Want to place the code here that changes the first_screen text to "Hi I'm The Fifth Screen"
        Clock.schedule_once(self.screen_switch_one, 10)
        return root_widget

    def screen_switch_one(a, b):
        root_widget.current = '_first_screen_'

    def screen_switch_two(a, b):
        root_widget.current = '_second_screen_'

    def screen_switch_three(a, b):
        root_widget.current = '_third_screen_'

    def screen_switch_four(a, b):
        root_widget.current = '_fourth_screen_'

if __name__ == '__main__':
    SwitchingScreenApp().run()