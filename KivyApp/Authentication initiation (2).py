#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 30 10:35:11 2019

@author: foviatech1
"""


import kivy
import numpy
#import kivymd
#import kivymd.toolbar.Toolbar

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput

#from kivy.lang import Builder
#from kivy.properties import ObjectProperty, StringProperty
#from kivymd.theming import ThemeManager
#from kivymd.navigationdrawer import NavigationDrawer

class TestApp(App):
    def build(self):
   
    
        return Button(text='Detected')



class LoginScreen(GridLayout):

    def __init__(self, **kwargs):
        super(LoginScreen, self).__init__(**kwargs)
        self.cols = 2
        self.add_widget(Label(text='User Name'))
        self.username = TextInput(multiline=False)
        self.add_widget(self.username)
        self.add_widget(Label(text='password'))
        self.password = TextInput(password=True, multiline=False)
        self.add_widget(self.password)


class MyApp(App):

    def build(self):
        return LoginScreen()
    
if __name__ == '__main__':
    MyApp().run()
    
    
#
#main_widget_kv = '__main__'
#
#class Navigator(NavigationDrawer):
#    image_source = StringProperty('images/me.png')
#
#class NavigateApp(App):
#    theme_cls = ThemeManager()
#    nav_drawer = ObjectProperty()
#
#    def build(self):
#        main_widget = Builder.load_string(main_widget_kv)
#        self.nav_drawer = Navigator()
#        return main_widget
#
#NavigateApp().run()

TestApp().run()



